<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;

class UserController extends Controller
{
    public function allUsers() {
        try {
            $users = User::all();
            return self::ApiResponse(self::SUCCESS, 'Fetch all users.', $users);
        } catch (\Exception $exception) {
            return self::ApiResponse(self::ERROR, $exception->getMessage());
        }
    }

    public function createUser() {
        try {
            $data = Request::input('data');
            $user = new User();
            $user->password = Hash::make($data['password']);
            $user->email = $data['email'];
            $user->name = $data['name'];
            $user->rfid_number = $data['rfid_number'];
            $user->user_level = 1;
            $user->user_status = 1;
            $id = $user->save();
            return self::ApiResponse(self::SUCCESS, 'Create new user.', $id);
        } catch (\Exception $exception) {
            return self::ApiResponse(self::ERROR, $exception->getMessage());
        }
    }

    public function userByRfid($rfid="") {
        try {
            $user = User::where('rfid_number', $rfid)->get();
            return self::ApiResponse(self::SUCCESS, 'User By RFID.', $user);
        } catch (\Exception $exception) {
            return self::ApiResponse(self::ERROR, $exception->getMessage());
        }
    }
}
