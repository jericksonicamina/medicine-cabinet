<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Response;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    const SUCCESS = "SUCCESS";
    const FAILED = "FAILED";
    const WARNING = "WARNING";
    const ERROR = "ERROR";

    public function ApiResponse($status, $message, $data = []) {
        $response = [
            'type' => $status,
            'message' => $status . ': ' . $message,
        ];
        if ($status == self::SUCCESS || $status == self::WARNING) {
            $response['data'] = $data;
        }
        return Response::json($response);
    }
}
