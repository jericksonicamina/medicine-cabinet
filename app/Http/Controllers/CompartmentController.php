<?php

namespace App\Http\Controllers;

use App\Compartment;
use App\User;
use Illuminate\Support\Facades\Request;

class CompartmentController extends Controller
{
    public function compartments() {
        try {
            $compartments = Compartment::all();
            return self::ApiResponse(self::SUCCESS, 'Fetch all compartments.', [$compartments]);
        } catch (\Exception $exception) {
            return self::ApiResponse(self::ERROR, $exception->getMessage());
        }
    }

    public function compartmentById($id) {
        try {
            $compartment = Compartment::where('compartment_id', $id)->get();
            return self::ApiResponse(self::SUCCESS, 'Fetch all compartments.', $compartment);
        } catch (\Exception $exception) {
            return self::ApiResponse(self::ERROR, $exception->getMessage());
        }
    }

    public function compartmentsByRFID($rfid) {
        try {
            $user = User::select('user_id')->where('rfid_number', $rfid)->get();
            $compartments = Compartment::where('user_id', $user[0]->user_id)->get();
            return self::ApiResponse(self::SUCCESS, 'Fetch all compartments.', [$compartments]);
        } catch (\Exception $exception) {
            return self::ApiResponse(self::ERROR, $exception->getMessage());
        }
    }

    public function createCompartment() {
        try {
            $user_id = Request::input('user_id');
            $user = new Compartment();
            $user->user_id = $user_id;
            $user->status = 1;
            $id = $user->save();
            return self::ApiResponse(self::SUCCESS, 'Create new compartment.', $id);
        } catch (\Exception $exception) {
            return self::ApiResponse(self::ERROR, $exception->getMessage());
        }
    }
}
