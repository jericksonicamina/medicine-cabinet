<?php

namespace App\Http\Controllers;

use App\Medicine;
use App\Notification;
use Illuminate\Support\Facades\Request;

class MedicineController extends Controller
{
    public function createMedicineRecord() {
        try {
            $data = Request::input('data');
            $medicine = new Medicine();
            $medicine->compartment_id = $data['compartment_id'];
            $medicine->medicine_name = $data['medicine_name'];
            $medicine->medicine_count = $data['medicine_count'];
            $medicine->medicine_schedule = $data['medicine_schedule'];
            $medicine->medicine_schedule_initial = $data['medicine_schedule_initial'];
            $id = $medicine->save();
            return self::ApiResponse(self::SUCCESS, 'Create new medicine record.', $id);
        } catch (\Exception $exception) {
            return self::ApiResponse(self::ERROR, $exception->getMessage());
        }
    }

    public function medicineRecords() {
        try {
            $compartments = Medicine::all();
            return self::ApiResponse(self::SUCCESS, 'Fetch all medicine records.', [$compartments]);
        } catch (\Exception $exception) {
            return self::ApiResponse(self::ERROR, $exception->getMessage());
        }
    }

    public function medicineRecordByCompartmentID($compartment_id) {
        try {
            $compartments = Medicine::where('compartment_id', $compartment_id)->get();
            return self::ApiResponse(self::SUCCESS, 'Fetch medicine record by compartment id.', $compartments);
        } catch (\Exception $exception) {
            return self::ApiResponse(self::ERROR, $exception->getMessage());
        }
    }

    public function updateMedicineRecord($medicine_name, $medicine_count, $medicine_schedule, $compartment_id) {
        try {
            $compartments = Medicine::where('compartment_id', $compartment_id)
                ->update([
                    'medicine_name' => $medicine_name,
                    'medicine_count' => $medicine_count,
                    'medicine_schedule' => $medicine_schedule
                ]);
            return self::ApiResponse(self::SUCCESS, 'Fetch medicine record by compartment id.', $compartments);
        } catch (\Exception $exception) {
            return self::ApiResponse(self::ERROR, $exception->getMessage());
        }
    }

    public function availableMedicineRecords() {
        try {
            $medicines = Medicine::all();
            foreach ($medicines as $medicine) {
                $schedule = floor(time());
                $computed_schedule = $medicine['medicine_schedule'] - $schedule;
            }
            return self::ApiResponse(self::SUCCESS, 'Fetch all medicine records.', [$medicines]);
        } catch (\Exception $exception) {
            return self::ApiResponse(self::ERROR, $exception->getMessage());
        }
    }
}
