<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function createNotification($rfid, $notification_action) {
        try {
            $notification = new Notification();
            $notification->rfid = $rfid;
            $notification->notification_action = $notification_action;
            $notification->status = 1;
            $id = $notification->save();
            return self::ApiResponse(self::SUCCESS, 'Create new notification.', $id);
        } catch (\Exception $exception) {
            return self::ApiResponse(self::ERROR, $exception->getMessage());
        }
    }

    public function notificationByRFID($rfid) {
        try {
            $notification = Notification::where('rfid', $rfid)->get();
            return self::ApiResponse(self::SUCCESS, 'Fetch all notifications.', [$notification]);
        } catch (\Exception $exception) {
            return self::ApiResponse(self::ERROR, $exception->getMessage());
        }
    }

    public function updateNotification() {
        try {
            $notification = Notification::where('status', 1)->update(['status' => 2]);
            return self::ApiResponse(self::SUCCESS, 'Fetch all notifications.', [$notification]);
        } catch (\Exception $exception) {
            return self::ApiResponse(self::ERROR, $exception->getMessage());
        }
    }
}
