<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Compartment extends Model
{
    use Notifiable;

    protected $fillable = [
        'user_id', 'status'
    ];
}
