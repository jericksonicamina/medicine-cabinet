<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Medicine extends Model
{
    use Notifiable;

    protected $fillable = [
        'compartment_id', 'medicine_name', 'medicine_count', 'medicine_schedule', 'medicine_schedule_initial'
    ];
}
