<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDispenseHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispense_history', function (Blueprint $table) {
            $table->id('dispense_history_id');
            $table->foreignId('user_id');
            $table->string('compartment');
            $table->string('medicine_name');
            $table->integer('medicine_recorded_count');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispense_history');
    }
}
