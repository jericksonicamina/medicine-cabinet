<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/users', 'UserController@allUsers');
Route::get('/user/{rfid?}', 'UserController@userByRfid');
Route::post('/create-user', 'UserController@createUser');
Route::get('/compartments', 'CompartmentController@compartments');
Route::get('/compartment/{id?}', 'CompartmentController@compartmentById');
Route::get('/compartments/user-rfid/{rfid?}', 'CompartmentController@compartmentsByRFID');
Route::post('/create-compartment', 'CompartmentController@createCompartment');
Route::get('/notification/add/{rfid?}/{notification_action?}', 'NotificationController@createNotification');
Route::get('/notification-active-by-rfid/{rfid?}', 'NotificationController@notificationByRFID');
Route::get('/notification/update', 'NotificationController@updateNotification');
Route::post('/create-medicine-record', 'MedicineController@createMedicineRecord');
Route::get('/medicines', 'MedicineController@medicineRecords');
Route::get('/medicine/{compartment_id?}', 'MedicineController@medicineRecordByCompartmentID');
Route::get('/medicine-update/{medicine_name?}/{medicine_count?}/{medicine_schedule?}/{compartment_id?}', 'MedicineController@updateMedicineRecord');
Route::get('/available-medicines', 'MedicineController@availableMedicineRecords');
